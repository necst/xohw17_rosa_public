# HW Integration Module

This folder contains the code infrastructure that is about the integration with HW tools in the ROSA project.
The current version provides a Static Code Analysis tool a little bit different w.r.t. the one in the folder "Static Analyzer" 
and more oriented to HW integration stuff.
The language currently supported is a subset of the C-99 language, whose precise specification will be made available soon.
At the moment the tool is also able to correctly parse VivadoHLS reports provided in an XML format.
