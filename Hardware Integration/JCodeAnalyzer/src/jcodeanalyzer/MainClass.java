/***/
package jcodeanalyzer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jcodeanalyzer.analyzer.OperationalIntensityCalculator;
import jcodeanalyzer.help.LanguageConfiguration;
import jcodeanalyzer.model.Program;
import jcodeanalyzer.parser.AbstractParser;
import jcodeanalyzer.parser.ProgramParser;

/**
 * @author Francesco Bertelli
 * @see String
 */
public class MainClass 
{

    // Static Methods
    
    /**
     * @param args the command line arguments
     */
    public static void main ( String [] args ) 
    {
        List < String > sourceLines ;
        AbstractParser programParser ;
        LanguageConfiguration languagConfiguration ;
        OperationalIntensityCalculator operationalIntensityCalculator ;
        Path sourcePath ;
        Program program ;
        String operationalIntensity ;
        String sourceContent ;
        String sourceName ;
        sourceName = "source1.c" ;
        try 
        {
            sourcePath = Paths.get ( sourceName , args ) ;    
            sourceLines = Files.readAllLines ( sourcePath ) ;
            sourceContent = stringify ( sourceLines ) ;
            languagConfiguration = new LanguageConfiguration ( new String [] { "for" , "do" , "while" , "if" , "else" , "switch" , "case" , "break" } , 
                                               new String [] { "char" , "byte" , "short" , "int" , "long" , "float" , "double" } ) ;
            program = new Program ( 0 ) ;
            programParser = new ProgramParser ( languagConfiguration , program , sourceContent ) ;
            programParser.parse () ;
            System.out.println ( program ) ;
            operationalIntensityCalculator = new OperationalIntensityCalculator () ;
            operationalIntensity = operationalIntensityCalculator.calculate ( program ) ;
            System.out.println ( "Operational Intensity : " + operationalIntensity ) ;
        } 
        catch ( IOException ex ) 
        {
            Logger.getLogger ( MainClass.class.getName () ).log ( Level.SEVERE , null, ex ) ;
        }
        /*VivadoHLSReportParser reportParser ;
        reportParser = new VivadoHLSReportParser () ;
        VivadoHLSReport report ;
        report = reportParser.parse("f_csynth.xml");*/
        
    }
    
    /**
     * @param in
     * @return
     */
    private static String stringify ( List < String > in )
    {
        String res ;
        StringBuffer sb ;
        sb = new StringBuffer ( in.size () ) ;
        in.stream ().forEach ( ( s ) -> { sb.append ( s ) ; } ) ;
        res = sb.toString () ;
        return res ;
    }
}
