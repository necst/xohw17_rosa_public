/***/

package jcodeanalyzer.model;

/**
 *
 * @author Francesco Bertelli
 */
public class SimpleStatement 
    implements IFunctionElement
{
 
    // Instance Fields
   
    /***/
    protected String content ;
    
    // Instance Methods
    
    /**
     * @param content 
     * @throws IllegalArgumentException
     */
    public SimpleStatement ( String content ) 
    {
        if ( content != null )
            this.content = content ;
        else
            throw new IllegalArgumentException () ;
    }
    
    /**
     * AS THE SUPER'S ONE.
     */
    @Override ()
    public String toString () 
    {
        String res ;
        StringBuffer sb ;
        sb = new StringBuffer ( content.length () ) ;
        sb.append ( "Simple Statement : " ) ;
        sb.append ( content ) ;
        res = sb.toString () ; 
        return res ;
    }
    
}
