/***/

package jcodeanalyzer.model;

/**
 *
 * @author Francesco Bertelli
 */
public class Assignment 
    extends SimpleStatement
{
 
    // Instance Methods
    
    /**
     * @param content 
     */
    public Assignment ( String content ) 
    {
        super ( content ) ;
    }
    
}
