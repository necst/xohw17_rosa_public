/***/

package jcodeanalyzer.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Francesco Bertelli
 * @see IProgramElement
 * @see List
 * @see IParsableElement
 */
public class Program 
    implements IParsableElement
{

    // Instance Fields
    
    /**
     * List the global variables
     */
    private final Map < String , Variable > globalVariables ;
    
    /***/
    private final List < IProgramElement > statements ;
    
    // Instance Methods
    
    /**
     * @param estimatedNumberOfStatements 
     */
    public Program ( Integer estimatedNumberOfStatements ) 
    {
        statements = new ArrayList <> ( estimatedNumberOfStatements ) ;
        globalVariables = new HashMap <> ( ( int ) ( Math.sqrt ( estimatedNumberOfStatements ) ) ) ;
    }
    
    /**
     * AS THE SUPER'S ONE.
     */
    @Override ()
    public String toString () 
    {
        String res ;
        StringBuffer sb ;
        sb = new StringBuffer ( statements.size () ) ;
        sb.append ( "Global Variables \n" ) ;
        globalVariables.forEach ( ( s , v ) -> { sb.append ( v.toString () ) ; sb.append ( "\n" ) ; } );
        statements.forEach ( e -> { sb.append ( e.toString () ) ; sb.append ( "\n" ) ; } ) ;
        res = sb.toString () ;
        return res ;
    }
    
    /**
     * @param v
     * @throws IllegalArgumentException
     */
    public void addGlobalVariable ( Variable v ) 
    {
        if ( v != null )
            globalVariables.put ( v.getName () , v ) ; 
        else
            throw new IllegalArgumentException () ;
    }
    
    /**
     * @param statement 
     * @throws IllegalArgumentException
     */
    public void appendStatement ( IProgramElement statement ) 
    {
        if ( statement != null )
            statements.add ( statement ) ;
        else
            throw new IllegalArgumentException () ;
    }
    
    /**
     * @return
     */
    public Iterator < IProgramElement > statements () 
    {
        return statements.iterator () ;
    }
    
    /***/
    public boolean isGlobalVariable ( String name )
    {
        boolean res ;
        res = globalVariables.keySet ().contains ( name ) ;
        return res ;
    }
    
}
