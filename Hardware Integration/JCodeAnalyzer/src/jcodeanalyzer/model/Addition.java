/***/

package jcodeanalyzer.model;

/**
 *
 * @author Francesco Bertelli
 */
public class Addition 
    extends SimpleStatement
{
    
    // Instance Methods
    
    /**
     * @param content 
     */
    public Addition ( String content ) 
    {
        super ( content ) ;
    }
    
    /**
     * AS THE SUPER'S ONE.
     */
    @Override ()
    public String toString () 
    {
        String res ;
        StringBuffer sb ;
        sb = new StringBuffer ( content.length () ) ;
        sb.append( "Addition Statement : " ) ;
        sb.append ( content ) ;
        res = sb.toString () ;
        return res ;
    }
    
}
