/***/

package jcodeanalyzer.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Francesco Bertelli
 */
public class Function 
    implements IProgramElement
{

    // Instance Fields

    /***/
    private final List < Variable > prototypeVariables ;
    
    /***/
    private final List < IFunctionElement > statements ;

    /***/
    private final String name ;
    
    /***/
    private final String returnType ;
    
    // Instance Methods
    
    /**
     * @param name 
     * @param returnType 
     * @throws IllegalArgumentException
     */
    public Function ( String name , String returnType ) 
    {
        if ( name != null && returnType != null )
        {
            this.name = name ;
            this.returnType = returnType ;
            prototypeVariables = new ArrayList <> ( 0 ) ;
            statements = new ArrayList <> ( 0 ) ;
        }
        else
            throw new IllegalArgumentException () ;
    }
    
    /**
     * AS THE SUPER'S ONE.
     */
    @Override
    public String toString () 
    {
        String res ;
        StringBuffer sb ;
        sb = new StringBuffer ( prototypeVariables.size () + statements.size () ) ;
        sb.append ( "Function Name : " ) ;
        sb.append ( name ) ;
        sb.append ( "\n" ) ;
        sb.append ( "Return Type : " ) ;
        sb.append ( returnType ) ;
        sb.append ( "\n" ) ;
        sb.append ( "Prototype Variables :" ) ;
        sb.append ( "\n" ) ;
        prototypeVariables.forEach ( v -> { sb.append ( "\t" ) ; sb.append ( v.toString () ) ; sb.append ( "\n" ) ; } ) ;
        sb.append ( "Function Body :" ) ;
        sb.append ( "\n" ) ;
        statements.forEach ( s -> { sb.append ( "\t" ) ; sb.append ( s.toString () ) ; sb.append ( "\n" ) ; } );
        res = sb.toString () ;
        return res ;
    }

    /**
     * @param variable 
     * @throws IllegalArgumentException
     */
    public void addPrototypeVariable ( Variable variable )
    {
        if ( variable != null )
            prototypeVariables.add ( variable ) ;
        else
            throw new IllegalArgumentException () ;
    }
    
    /**
     * @param statement 
     * @throws IllegalArgumentException
     */
    public void addStatement ( IFunctionElement statement )
    {
        if ( statement != null )
            statements.add ( statement ) ;
        else
            throw new IllegalArgumentException () ;
    }
    
    /**
     * @return
     */
    public Iterator < IFunctionElement > statements () 
    {
        return statements.iterator () ;
    }
        
}
