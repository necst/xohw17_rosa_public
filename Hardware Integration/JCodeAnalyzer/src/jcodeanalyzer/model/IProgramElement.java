/***/

package jcodeanalyzer.model;

/**
 *
 * @author Francesco Bertelli
 */
public interface IProgramElement extends IParsableElement {}
