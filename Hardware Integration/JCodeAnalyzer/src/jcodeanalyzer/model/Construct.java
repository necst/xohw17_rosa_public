/***/

package jcodeanalyzer.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Francesco Bertelli
 * @see String
 */
public class Construct 
    implements IFunctionElement , IProgramElement   
{

    //ADD DEPENDENCY IN UML
    
    // Instance Fields
    
    /***/
    private final List < IFunctionElement > statements ;
    
    /***/
    private final List < IFunctionElement > conditionStatements ;
    
    /***/
    private String body ;
    
    /***/
    private String condition ;
    
    /***/
    private String constructName ;
    
    // Instance Methods
    
    /**
     * @param body 
     * @param condition
     * @param constructName
     * @throws IllegalArgumentException
     */
    public Construct ( String body , String condition , String constructName )
    {
        if ( body != null && condition != null && constructName != null )
        {
            this.body = body.trim () ;
            this.condition = condition.trim () ;
            this.constructName = constructName.trim () ;
            statements = new ArrayList <> ( 1 ) ;
            conditionStatements = new ArrayList <> ( 2 ) ;
        }
        else
            throw new IllegalArgumentException () ;
    }
    
    /**
     * AS THE SUPER'S ONE.
     */
    @Override
    public String toString () 
    {
        String res ;
        StringBuffer sb ;
        sb = new StringBuffer ( 10 ) ;
        sb.append ( "Construct Name = " ) ;
        sb.append ( constructName ) ;
        sb.append ( ", Condition = " ) ;
        sb.append ( condition ) ;
        sb.append ( ", body = " ) ;
        sb.append ( body ) ;
        sb.append ( "\n" ) ;
        statements.forEach ( e -> { sb.append ( e.toString () ) ; sb.append ( "\n" ) ; } ) ;
        res = sb.toString () ;
        return res ;
    }
    
    /**
     * @param statement 
     * @throws IllegalArgumentException
     */
    public void addStatement ( IFunctionElement statement ) 
    {
        if ( statement != null )
            statements.add ( statement ) ;
        else
            throw new IllegalArgumentException () ;
    }
    
    /**
     * @param statement 
     * @throws IllegalArgumentException
     */
    public void addConditionStatement ( IFunctionElement statement )
    {
        if ( statement != null )
            conditionStatements.add ( statement ) ;
        else
            throw new IllegalArgumentException () ;
    }
    
    /**
     * Getter
     * 
     * @return
     */
    public String getBody () 
    {
        return body ;
    }
    
    /**
     * Getter
     * 
     * @return 
     */
    public String getCondition () 
    {
        return condition ;
    }
    
    /**
     * Getter
     * 
     * @return 
     */
    public String getConstructName () 
    {
        return constructName ;
    }
    
    /***/
    public Iterator < IFunctionElement > statements () 
    {
        return statements.iterator () ;
    }
    
}
