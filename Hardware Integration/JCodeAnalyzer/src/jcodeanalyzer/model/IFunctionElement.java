/***/

package jcodeanalyzer.model;

/**
 *
 * @author Francesco Bertelli
 */
public interface IFunctionElement extends IParsableElement {}
