/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcodeanalyzer.model;

/**
 *
 * @author parallels
 */
public class Subtraction extends SimpleStatement
{
    
    // Instance Methods
    
    /**
     * @param content 
     */
    public Subtraction ( String content ) 
    {
        super ( content ) ;
    }
    
    /**
     * AS THE SUPER'S ONE.
     */
    @Override ()
    public String toString () 
    {
        String res ;
        StringBuffer sb ;
        sb = new StringBuffer ( content.length () ) ;
        sb.append( "Subtraction Statement : " ) ;
        sb.append ( content ) ;
        res = sb.toString () ;
        return res ;
    }
    
}
