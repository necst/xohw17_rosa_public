/***/

package jcodeanalyzer.model;

/**
 *
 * @author Francesco Bertelli
 * @see Boolean
 * @see String
 */
public class Variable 
    implements IFunctionElement , IProgramElement 
{

    /***/
    private Boolean array ;
    
    /***/
    private String name ;
    
    /***/
    private String type ;
    
    /***/
    private String multiplicity ;
    
    private String initialValue ;
    
    /**
     * @param array 
     * @param multiplicity 
     * @param name 
     * @param type 
     * @param initialValue 
     * @throws IllegalArgumentException
     */
    public Variable ( Boolean array , String name , String type , String multiplicity , String initialValue ) 
    {
        if ( name != null && type != null && multiplicity != null )
        {
            this.array = array ;
            this.name = name ;
            this.type = type ;
            this.multiplicity = multiplicity ;
            this.initialValue = initialValue ;
        }
        else
            throw new IllegalArgumentException () ;
    }
    
    /**
     * AS THE SUPER'S ONE.
     */
    @Override
    public String toString () 
    {
        String res ;
        StringBuffer s ;
        s = new StringBuffer ( 25 ) ;
        s.append ( "Variable Declaration: name = ") ;
        s.append ( name ) ;
        s.append ( ", type = " ) ;
        s.append ( type ) ;
        s.append ( ", is array = " ) ;
        s.append ( array ) ;
        s.append ( ", multiplicity = " ) ;
        s.append ( multiplicity ) ;
        res = s.toString () ;
        return res ;
    }
    
    public String getName () 
    {
        return name ;
    }
    
}
