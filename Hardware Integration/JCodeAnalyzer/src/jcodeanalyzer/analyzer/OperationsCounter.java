/***/

package jcodeanalyzer.analyzer;

import java.util.Iterator;
import jcodeanalyzer.model.Assignment;
import jcodeanalyzer.model.Construct;
import jcodeanalyzer.model.Function;
import jcodeanalyzer.model.IFunctionElement;
import jcodeanalyzer.model.IProgramElement;
import jcodeanalyzer.model.Program;
import jcodeanalyzer.model.SimpleStatement;

/**
 *
 * @author Francesco Bertelli
 */
public class OperationsCounter 
{

    // Instance Methods
    
    /***/
    public OperationsCounter () {}
    
    /**
     * @param program 
     * @return 
     */
    public String calculate ( Program program ) 
    {
        Iterator < IProgramElement > statements ;
        StringBuffer sb ;
        String res ;
        statements = program.statements();
        sb = new StringBuffer ( 1 ) ;
        statements.forEachRemaining ( e -> { String r ; r = OperationsCounter.this.analyzeProgramElement ( e ) ; if ( ! r.isEmpty () ) { ; sb.append ( r ) ;  } } ) ; 
        res = sb.toString () ;
        return res ;
    }
    
    /**
     * @param e
     */
    private String analyzeProgramElement ( IProgramElement e )
    {
        Iterator < IFunctionElement > statements ;
        String res ;
        StringBuffer sb ;
        sb = new StringBuffer ( 1 ) ;
        if ( e instanceof Function )
        {
            statements = ( ( Function ) e ).statements () ;
            statements.forEachRemaining ( ee -> { String r ; r = analyzeProgramElement ( ee ) ; if ( ! r.isEmpty () ) { sb.append ( "+" ) ; sb.append ( r ) ; } } ) ;
        }
        res = sb.toString () ;
        return res ;
    }
    
    /***/
    private String analyzeProgramElement ( IFunctionElement e )
    {
        Iterator < IFunctionElement > statements ;
        String res ;
        String condition ;
        StringBuffer sb ;
        sb = new StringBuffer ( 1 ) ;
        if ( e instanceof SimpleStatement && ! ( e instanceof Assignment ) )
        {
            sb.append ( "1" ) ;
        }
        else
            if ( e instanceof Construct )
                switch ( ((Construct) e).getConstructName ().trim ().toUpperCase () )
                {
                    case "FOR" :
                    case "DO" :
                        condition = ( ( Construct ) e ).getCondition () ;
                        if ( condition.indexOf(";")!=-1)
                            condition = condition.split(";")[1];
                        System.out.println("||||||||||||||         "+condition);                        
                        statements = ( ( Construct ) e ).statements () ;
                        sb.append ( condition.trim ().split ( " " )[2] ) ;
                        sb.append( " * " ) ;
                        sb.append ( "( " ) ;
                        if ( ((Construct) e).getConstructName ().trim ().toUpperCase ().compareToIgnoreCase("FOR") == 0 )
                            sb.append("1 " ) ;
                        statements.forEachRemaining ( ee -> { String temp = analyzeProgramElement ( ee ) ; if ( ! temp.isEmpty () ) { sb.append ( "+" ) ; sb.append ( temp ) ; } } ) ;
                        sb.append ( " )" ) ;
                    break ;
                    default :
                        sb.append ( "" ) ;
                }
        res = sb.toString () ;
        return res ;
    }
    
}
