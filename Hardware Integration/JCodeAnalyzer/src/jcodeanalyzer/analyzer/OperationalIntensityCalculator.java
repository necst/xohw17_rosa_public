/***/

package jcodeanalyzer.analyzer;

import jcodeanalyzer.model.Program;

/**
 *
 * @author Francesco Bertelli
 */
public class OperationalIntensityCalculator 
{
    
    // Instance Methods
    
    /***/
    public OperationalIntensityCalculator () {}
    
    /**
     * @param program 
     * @return 
     */
    public String calculate ( Program program ) 
    {
        MemoryTransferCounter memoryTransferCounter ;
        OperationsCounter operationsCounter ;
        String res ;
        String memoryTransfers ;
        String operations ;
        if ( program != null )
        {
            memoryTransferCounter = new MemoryTransferCounter () ;
            operationsCounter = new OperationsCounter () ;
            operations = operationsCounter.calculate ( program ) ;
            memoryTransfers = memoryTransferCounter.calculate ( program ) ;
            res = operations + " / " + memoryTransfers ;
            return res ;
        }
        else
            throw new IllegalArgumentException () ;
    }
    
}
