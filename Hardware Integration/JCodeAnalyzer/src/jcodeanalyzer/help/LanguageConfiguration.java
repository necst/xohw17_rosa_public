/***/
package jcodeanalyzer.help;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 * @author Francesco Bertelli
 * @see Boolean
 * @see Collection
 * @see String
 */
public class LanguageConfiguration 
{
    
    // Instance Fields
    
    /***/
    private List < String > constructs ;
    
    /***/
    private List < String > primitiveTypes ;
    
    // Instance Methods
    
    /**
     * @param constructs 
     * @param primitiveTypes 
     * @throws IllegalArgumentException
     */
    public LanguageConfiguration ( String [] constructs , String [] primitiveTypes ) 
    {
        if ( constructs != null && primitiveTypes != null )
        {
            this.constructs = new ArrayList <> ( constructs.length ) ;
            this.primitiveTypes = new ArrayList <> ( primitiveTypes.length ) ;
            Stream.of ( constructs ).parallel ().forEach ( s -> this.constructs.add ( s ) ) ; 
            Stream.of ( primitiveTypes ).parallel ().forEach ( s -> this.primitiveTypes.add ( s ) ) ; 
        }
        else
            throw new IllegalArgumentException () ;
    }
    
    /**
     * @return
     */
    public String getArrayCloseDelimiter () 
    {
        return "]" ;
    }
    
    /**
     * @return
     */
    public String getArrayOpenDelimiter () 
    {
        return "[" ;
    }
    
    /**
     * @return
     */
    public String getBlockEndDelimiter () 
    {
        return "}" ;
    }
    
    /**
     * @return
     */
    public String getBlockStartDelimiter () 
    {
        return "{" ;
    }
    
    /**
     * @return
     */
    public String getConditionEndDelimiter () 
    {
        return ")" ;
    }
    
    /**
     * @return 
     */
    public String getConditionStartDelimiter () 
    {
        return "(" ;
    }
    
    /**
     * @return 
     */
    public String getFunctionEndDelimiter () 
    {
        return "}" ;
    }
    
    /**
     * @return
     */
    public String getFunctionStartDelimiter () 
    {
        return "{" ;
    }
        
    /**
     * @return
     */
    public String getStatementEndDelimiter () 
    {
        return ";" ;
    }
    
    /**
     * @return
     */
    public String getVariableDeclarationEndDelimiter () 
    {
        return ";" ;
    }
    
    
    /**
     * @param s
     * @return 
     */
    public Boolean isConstruct ( String s ) 
    {
        boolean res ;
        res = constructs.contains ( s ) ;
        return res ;
    }
    
    /**
     * @param s
     * @return
     */
    public Boolean isPrimitiveType ( String s ) 
    {
        boolean res ;
        res = primitiveTypes.contains ( s ) ;
        return res ;
    }
    
}
