/***/

package jcodeanalyzer.parser;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jcodeanalyzer.help.LanguageConfiguration;
import jcodeanalyzer.model.Program;

/**
 *
 * @author Francesco Bertelli
 */
public class ProgramParser 
    extends AbstractParser
{
    
    // Static Fields
    
    /***/
    private static final Logger LOGGER = Logger.getLogger ( ProgramParser.class.getName () ) ;
    
    // Instance Fields
    
    /***/
    private int currentPointer ;
    
    /***/
    private int tempCurrentPointer ;
    
    // Instance Methods
   
    /**
     * @param languageConfiguration 
     * @param program 
     * @param content 
     */
    public ProgramParser ( LanguageConfiguration languageConfiguration , Program program , String content ) 
    {
        super ( languageConfiguration , program , content ) ;
        currentPointer = 0 ;
        tempCurrentPointer = 0 ;
    }
    
    /**
     * AS THE SUPER'S ONE.
     */
    @Override
    public List < Program > parse () 
    {
        /*Function function ;
        FunctionParser functionParser ;
        LOGGER.log ( Level.INFO , "Start Parsing Program\n" );
        parseGlobalVariables () ;
        functionParser = new FunctionParser ( getLanguageConfiguration () , getProgram () , getContent ().substring ( currentPointer ) ) ;
        function = functionParser.parse ().get ( 0 ) ;
        getProgram ().appendStatement ( function ) ;
        LOGGER.log ( Level.INFO , "Stop Parsing Program\n" ) ;*/
        char next ;
        String s ;
        LOGGER.log ( Level.INFO , "Start Parsing Program\n" );
        do 
        {
            next = reachNextElement () ; 
            switch ( next )
            {
                case 'D' :
                    DefineParser defineParser ;
                    s = getContent ().substring ( currentPointer , tempCurrentPointer ).trim () ;
                    defineParser = new DefineParser ( getLanguageConfiguration () , getProgram () , s ) ;
                    currentPointer = tempCurrentPointer ;
                    getProgram ().addGlobalVariable ( defineParser.parse ().get( 0 ) ) ; 
                    skipSpaces () ;
                break ;
                case 'F' :
                    FunctionParser functionParser ;
                    s = getContent ().substring ( currentPointer , tempCurrentPointer ).trim () ;
                    functionParser = new FunctionParser ( getLanguageConfiguration () , getProgram () , s ) ;
                    getProgram().appendStatement ( functionParser.parse ().get ( 0 ) ) ;
                    currentPointer = tempCurrentPointer ;
                    skipSpaces () ;
                break ;
                case 'V' :
                    VariableParser variableDeclarationParser ;
                    s = getContent ().substring ( currentPointer , tempCurrentPointer ).trim () ;
                    currentPointer = tempCurrentPointer ;
                    skipSpaces () ;
                    variableDeclarationParser = new VariableParser ( getLanguageConfiguration () , getProgram () , s ) ;
                    getProgram().addGlobalVariable ( variableDeclarationParser.parse ().get ( 0 ) ) ;
                break ;
            }
            skipSpaces () ;
        } 
        while ( next != 'N' && currentPointer < getContent ().trim ().length () ) ;
        return Collections.singletonList ( getProgram () ) ;
    }
    
    /***/
    private void skipSpaces () 
    {
        if ( currentPointer < getContent ().length () )
        {
            while ( getContent ().charAt ( currentPointer ) == ' ' )
                currentPointer ++ ;
            tempCurrentPointer = currentPointer ;
        }        
    }
    
    /***/
    private char reachNextElement ()
    {
        int numberOfSpaces ;
        String candidate ;
        char res = 'U';
        boolean found = false ;
        while ( ! found )
        {     
            candidate = getContent ().substring ( currentPointer , tempCurrentPointer ) ;
            if ( candidate.equals ( "#" ) )
            {   
                LOGGER.log ( Level.INFO , "Define Found" ) ;
                numberOfSpaces = 2 ;
                tempCurrentPointer = currentPointer ;         
                while ( numberOfSpaces > 0 )
                {  
                    while ( getContent ().charAt ( tempCurrentPointer ) != ' ' )
                        tempCurrentPointer ++ ;
                    while ( getContent ().charAt ( tempCurrentPointer ) == ' ' )
                        tempCurrentPointer ++ ;
                    numberOfSpaces -- ;
                }  
                while ( Character.isDigit ( getContent ().charAt ( tempCurrentPointer ) ) )
                    tempCurrentPointer ++ ;
                res = 'D' ;
                found = true ;

            }
            else
            {
                if ( getLanguageConfiguration ().isPrimitiveType( candidate ) )
                {
                    LOGGER.log ( Level.INFO , "Primitive Type Found : " + candidate ) ;
                    while ( getLanguageConfiguration ().getFunctionStartDelimiter ().charAt ( 0 ) != getContent ().charAt ( tempCurrentPointer ) &&
                            getLanguageConfiguration ().getStatementEndDelimiter ().charAt(0) != getContent ().charAt ( tempCurrentPointer ) ) 
                        tempCurrentPointer ++ ;
                    if ( getContent ().charAt ( tempCurrentPointer ) == getLanguageConfiguration ().getStatementEndDelimiter ().charAt ( 0 ) )
                    {
                        tempCurrentPointer ++ ;
                        res = 'V' ;
                        found = true ;
                    }
                    else
                        {
                            res = 'F' ;
                            found = true ;
                            int tmp = -1 ;
                            while ( getContent ().charAt ( tempCurrentPointer ) != getLanguageConfiguration ().getFunctionEndDelimiter ().charAt( 0 ) || tmp != 0 )
                            {
                                if ( getContent().charAt ( tempCurrentPointer ) == getLanguageConfiguration ().getFunctionStartDelimiter ().charAt ( 0 ) )
                                    tmp ++ ;
                                else
                                    if ( getContent().charAt ( tempCurrentPointer ) == getLanguageConfiguration ().getFunctionEndDelimiter ().charAt ( 0 ) )
                                        tmp -- ;
                                tempCurrentPointer ++ ;
                            }
                            tempCurrentPointer ++ ;
                        }
                        
                }
                else
                {
                    if ( tempCurrentPointer == getContent ().length () - 1 )
                    {
                        res = 'N' ;
                        found = true ;
                    }
                    else
                        tempCurrentPointer ++ ;
                }
            }
        }
        if ( res == 'U' )
            res = 'N' ;
        return res ;
    }
            
    
}
