/***/

package jcodeanalyzer.parser;

import java.util.List;
import jcodeanalyzer.help.LanguageConfiguration;
import jcodeanalyzer.model.IParsableElement;
import jcodeanalyzer.model.Program;

/**
 *
 * @author Francesco Bertelli
 * @see LanguageConfiguration
 * @see String
 */
public abstract class AbstractParser 
{    
    
    // Instance Fields
        
    /***/
    private final LanguageConfiguration languageConfiguration ;
    
    /***/
    private final Program program ;
    
    /**
     * The element to be parsed
     */
    private final String content ;
       
    // Instance Methods
    
    /**
     * @param languageConfiguration 
     * @param content the element to be parsed
     * @param program
     * @throws IllegalArgumentException
     */
    protected AbstractParser ( LanguageConfiguration languageConfiguration , Program program , String content )
    {
        if ( languageConfiguration != null && program != null && content != null )
        {
            this.languageConfiguration = languageConfiguration ;
            this.program = program ;
            this.content = content ;
        }
        else
            throw new IllegalArgumentException () ;
    }
    
    /**
     * @param <T>
     * @return
     */
    public abstract < T extends IParsableElement > List < T > parse () ;

    /**
     * Getter for the Content property.
     * 
     * @return
     */
    public String getContent () 
    {
        return content ;
    }
    
    /**
     * Getter for the LanguageConfiguration property.
     * 
     * @return
     */
    protected LanguageConfiguration getLanguageConfiguration () 
    {
        return languageConfiguration ;
    }
    
    /**
     * Getter for the Program property.
     * 
     * @return
     */
    protected Program getProgram () 
    {
        return program ;
    }
    
}
