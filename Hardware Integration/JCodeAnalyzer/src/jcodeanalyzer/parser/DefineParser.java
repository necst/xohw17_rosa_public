/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcodeanalyzer.parser;

import java.util.Collections;
import java.util.List;
import jcodeanalyzer.help.LanguageConfiguration;
import jcodeanalyzer.model.Program;
import jcodeanalyzer.model.Variable;

/**
 *
 * @author parallels
 */
public class DefineParser 
        extends AbstractParser
{
    
    // Instance Methods
    
    /**
     * @param languageConfiguration 
     * @param program 
     * @param content 
     */
    public DefineParser ( LanguageConfiguration languageConfiguration , Program program , String content ) 
    {
        super ( languageConfiguration , program , content ) ;
    }    
    
    /***/
    @Override
    public  List < Variable > parse() 
    {
        String [] tmp ;
        tmp = getContent ().split ( " " ) ;
        for ( int i = 0 ; i < tmp.length ; i ++ )
            tmp [ i ] = tmp [ i ].trim () ;
        Variable v ;
        v = new Variable ( false , tmp [ 0 ] , "int" , "1" , tmp [ 2 ] ) ;
        return Collections.singletonList ( v ) ;
    }
    
}
