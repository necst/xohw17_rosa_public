/***/

package jcodeanalyzer.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jcodeanalyzer.help.LanguageConfiguration;
import jcodeanalyzer.model.Construct;
import jcodeanalyzer.model.IFunctionElement;
import jcodeanalyzer.model.Program;

/**
 *
 * @author Francesco Bertelli
 * @see LanguageConfiguration
 * @see String
 */
public class ConstructParser 
    extends FunctionBodyParser  //not sure about this, not still in the UML 
{

    // Static Fields
    
    /***/
    private static final Logger LOGGER = Logger.getLogger ( ConstructParser.class.getName () ) ; 
    
    // Instance Fields
    
    /***/
    private Construct construct ;
    
    // Instance Methods
    
    /**
     * @param content 
     * @param languageConfiguration 
     * @param program 
     * @param name
     * @param condition 
     */
    public ConstructParser ( LanguageConfiguration languageConfiguration , Program program , String content , String name , String condition ) 
    {
        super ( languageConfiguration , program , content ) ;
        if ( name != null && condition != null )
        {
            construct = new Construct ( content , condition.trim().substring ( 1 , condition.length () - 1 ).trim () , name ) ;
            currentIndex = 0 ;
            temporaryCurrentIndex = 0 ;
        }
        else
            throw new IllegalArgumentException () ;
    }
    
    /**
     * AS THE SUPER'S ONE.
     */
    @Override
    public List < IFunctionElement > parse () 
    {
        List < IFunctionElement > res ;        
        List < IFunctionElement > temp ;
        AbstractParser otherElementParser ;
        String nextElementBody [] ; 
        String nextElementHead ;
        res = new ArrayList <> ( 1 ) ;
        LOGGER.log ( Level.INFO , "Start Parsing Construct\n" ) ;
        do
        {
            LOGGER.log ( Level.INFO , "Looking For Next Element\n" ) ;
            nextElementHead = getNextElementHead ().trim () ;    
            LOGGER.log ( Level.INFO , "Element Found : " + nextElementHead + "\n" ) ;
            if ( getLanguageConfiguration ().isPrimitiveType ( nextElementHead ) )
            {
                LOGGER.log ( Level.INFO , "Primitive Type Recognized\n" ) ;
                nextElementBody = getNextElementBodyVariable () ;
                LOGGER.log ( Level.INFO , "Variable Declaration Retrieved : " + nextElementBody [ 0 ] + "\n" ) ;
                while ( getContent ().charAt ( currentIndex ) == " ".charAt ( 0 ) )
                    currentIndex ++ ;
                temporaryCurrentIndex = currentIndex ;
                otherElementParser = new VariableParser ( getLanguageConfiguration () , getProgram () , nextElementHead.concat ( " " ).concat ( nextElementBody [ 0 ] ).trim () ) ;
            }
            else
                if ( getLanguageConfiguration ().isConstruct ( nextElementHead ) )
                {
                    LOGGER.log ( Level.INFO , "Construct Recognized\n" );
                    nextElementBody = getNextElementBodyConstruct ( nextElementHead ) ;
                    LOGGER.log ( Level.INFO , "Construct Retrieved: " + nextElementBody[1] + "\n" ) ;
                    currentIndex = temporaryCurrentIndex ;
                    otherElementParser = new ConstructParser ( getLanguageConfiguration () , getProgram () , nextElementBody [ 1 ] , nextElementHead , nextElementBody [ 0 ].trim () ) ;
                }
                else
                {
                    LOGGER.log ( Level.INFO , "Simple Statement Recognized\n" ) ;
                    nextElementBody = getNextElementBodySimpleStatement () ;
                    currentIndex = temporaryCurrentIndex ;
                    otherElementParser = new SimpleStatementParser ( getLanguageConfiguration () , getProgram () , nextElementHead.concat( nextElementBody [ 0 ] ) ) ;
                }
            temp = otherElementParser.parse () ;
            temp.forEach ( e -> construct.addStatement ( e ) ) ;
            LOGGER.log ( Level.INFO , "Element Parsing Successfully Finished\n" ) ;
            LOGGER.log ( Level.INFO , "Remaining Portion to parse : " + getContent ().substring ( currentIndex ) + "\n" ) ;
        }
        while ( currentIndex < getContent ().trim ().length () - 1 ) ;
        LOGGER.log ( Level.INFO , "End Parsing Construct\n" ) ;
        res.add ( construct ) ;
        return res ;
    }

}
