/***/
package jcodeanalyzer.parser;

import java.util.ArrayList;
import java.util.List;
import jcodeanalyzer.help.LanguageConfiguration;
import jcodeanalyzer.model.Program;
import jcodeanalyzer.model.Variable;

/**
 *
 * @author Francesco Bertelli
 * @see AbstractParser
 * @see LanguageConfiguration
 * @see String
 */
public class FunctionPrototypeParser 
    extends AbstractParser
{

    // Instance Methods
    
    /**
     * @param languageConfiguration 
     * @param content
     * @param program 
     */
    public FunctionPrototypeParser ( LanguageConfiguration languageConfiguration , Program program , String content ) 
    {
        super ( languageConfiguration , program , content ) ;
    }

    /**
     * AS THE SUPER'S ONE.
     */
    @Override
    public List < Variable > parse () 
    {
        List < Variable > res ;
        String [] tmp ;
        String [] tmppp ;
        String name ;
        String type ;
        String tmpp ;
        Variable v ;
        tmp = getContent ().split ( " " ) ;
        name = tmp [1].trim();
        type = tmp [0].trim () ;
        tmpp = getContent ().trim ().substring ( getContent().trim().indexOf ( getLanguageConfiguration ().getConditionStartDelimiter () ) + 1 , getContent ().trim ().indexOf ( getLanguageConfiguration ().getConditionEndDelimiter () ) ).trim() ;
        tmp = tmpp.split ( "," ) ;
        res = new ArrayList <> ( tmp.length ) ;
        if ( ! tmp[0].isEmpty() )
            for ( String s : tmp )
            {
                tmppp = s.trim ().split ( " " ) ;
                v = new Variable ( tmppp.length == 3 , 
                                   tmppp [ 1 ] , 
                                   tmppp [ 0 ], 
                                   tmppp.length == 3 ? tmppp [ 2 ].substring ( tmppp [ 2 ].indexOf ( "[" ) + 1 , tmppp [ 2 ].indexOf ( "]" ) ) : "1" , 
                                   null ) ; 
                res.add ( v ) ;
            }
        return null ;
    }
    
}
