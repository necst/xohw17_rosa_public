/***/
package jcodeanalyzer.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jcodeanalyzer.help.LanguageConfiguration;
import jcodeanalyzer.model.IFunctionElement;
import jcodeanalyzer.model.Program;

/**
 *
 * @author Francesco Bertelli
 * @see AbstractParser
 * @see LanguageConfiguration
 * @see String
 */
public class FunctionBodyParser 
    extends AbstractParser
{

    // Static Fields
    
    /***/
    private static final Logger LOGGER = Logger.getLogger ( FunctionBodyParser.class.getName () ) ; 
    
    // Instance Fields
    
    /***/
    protected Integer currentIndex ;
    
    /***/
    protected Integer temporaryCurrentIndex ;
    
    // Instance Methods
    
    /**
     * @param languageConfiguration 
     * @param content 
     * @param program 
     */
    public FunctionBodyParser ( LanguageConfiguration languageConfiguration , Program program , String content ) 
    {
        super( languageConfiguration , program , content ) ;
        currentIndex = 0 ;
        temporaryCurrentIndex = 0 ;
    }
    
    /**
     * AS THE SUPER'S ONE.
     */
    @Override
    public List < IFunctionElement > parse () 
    {
        String nextElementBody [] ; 
        List < IFunctionElement > res ;
        List < IFunctionElement > temp ;
        AbstractParser otherElementParser ;
        String nextElementHead ;
        res = new ArrayList <> ( 0 ) ;
        LOGGER.log ( Level.INFO , "Function Body Parser: Start Parsing Function Body " + getContent () + "\n" ) ;
        while ( currentIndex < getContent ().length () - 1 ) 
        {
            LOGGER.log ( Level.INFO , "Looking For Next Element\n" ) ;
            nextElementHead = getNextElementHead ().trim () ;
            LOGGER.log ( Level.INFO , "Element Found : " + nextElementHead ) ;
            if ( getLanguageConfiguration ().isPrimitiveType ( nextElementHead ) )
            {
                LOGGER.log ( Level.INFO , "Primitive Type Recognized\n" ) ;
                nextElementBody = getNextElementBodyVariable () ;
                LOGGER.log ( Level.INFO , "Variable Declaration Retrieved : " + nextElementBody [ 0 ] + "\n" ) ;
                while ( getContent ().charAt ( currentIndex ) == " ".charAt ( 0 ) )
                    currentIndex ++ ;
                temporaryCurrentIndex = currentIndex ;
                otherElementParser = new VariableParser ( getLanguageConfiguration () , getProgram () , nextElementHead.concat ( " " ).concat ( nextElementBody [ 0 ] ).trim () ) ;
            }
            else
                if ( getLanguageConfiguration ().isConstruct ( nextElementHead ) )
                {
                    LOGGER.log ( Level.INFO , "Construct Recognized : " + nextElementHead + "\n" );
                    nextElementBody = getNextElementBodyConstruct ( nextElementHead ) ;
                    LOGGER.log ( Level.INFO , "Construct Retrieved : " + nextElementBody [ 0 ] + "\n" ) ;
                    currentIndex = temporaryCurrentIndex ;
                    otherElementParser = new ConstructParser ( getLanguageConfiguration () , getProgram () , nextElementBody [ 1 ].substring ( 1 , nextElementBody [ 1 ].length () - 1 ).trim () , nextElementHead.trim () , nextElementBody [ 0 ].trim () ) ;
                }
                else
                {
                    LOGGER.log ( Level.INFO , "Simple Statement Recognized\n" ) ;
                    nextElementBody = getNextElementBodySimpleStatement () ;
                    currentIndex = temporaryCurrentIndex ;
                    otherElementParser = new SimpleStatementParser ( getLanguageConfiguration () , getProgram () , nextElementHead.concat( nextElementBody [ 0 ] ) ) ;
                }
            temp = otherElementParser.parse () ;
            temp.forEach ( e -> res.add ( e ) ) ;
            LOGGER.log ( Level.INFO , "Next Element Parsing Successfully Finished\n" ) ;
            LOGGER.log ( Level.INFO , "Remaining Portion to parse : " + getContent ().substring ( currentIndex ) + "\n" ) ;
        }
        
        LOGGER.log ( Level.INFO , "End Parsing Function Body\n" ) ;
        return res ;
    }
    
    /**
     * @param constructHead 
     * @return
     * @throws IllegalArgumentException
     */
    protected String [] getNextElementBodyConstruct ( String constructHead ) 
    {
        String res [] ;
        int counter ;
        switch ( constructHead.trim ().toUpperCase() )
        {
            case "FOR" :
                trim () ;
                counter = skipCondition () ;
                res = new String [ 2 ] ; 
                res [ 0 ] = getContent ().substring ( temporaryCurrentIndex , temporaryCurrentIndex + counter ) ;
                temporaryCurrentIndex += counter ;
                trim () ;
                reachOpenCurlyBrace () ;
                counter = skipBlock () ;
                res [ 1 ] = getContent ().substring ( temporaryCurrentIndex , temporaryCurrentIndex + counter ) ;
                res [ 1 ] = res [ 1 ].trim().substring ( 1 , res [ 1 ].trim ().length () - 1 ) ;
                temporaryCurrentIndex += counter ;
                break ;
            case "DO" :
                res = new String [ 2 ] ;
                trim () ;
                reachOpenCurlyBrace () ;
                counter = skipBlock () ;
                res [ 1 ] = getContent ().substring ( temporaryCurrentIndex , temporaryCurrentIndex + counter ) ;
                temporaryCurrentIndex += counter ;
                trim();
                // skip the while word
                temporaryCurrentIndex += 5 ;
                trim () ;                
                counter = skipCondition () ;
                res [ 0 ] = getContent ().substring ( temporaryCurrentIndex , temporaryCurrentIndex + counter ) ;
                temporaryCurrentIndex += counter ;
                while ( getContent ().charAt ( temporaryCurrentIndex ) != getLanguageConfiguration ().getStatementEndDelimiter ().charAt ( 0 ) )
                    temporaryCurrentIndex ++ ;
                temporaryCurrentIndex ++ ;
                trim () ;
            break ;
            default :
                throw new IllegalArgumentException () ;
        }
        return res ;
    }
    
    /**
     * @return
     */
    protected String [] getNextElementBodySimpleStatement () 
    {
        String res [] ;
        int temp ;
        res = new String [] { "" } ;
        temp = getContent ().indexOf ( getLanguageConfiguration ().getStatementEndDelimiter () , temporaryCurrentIndex ) ;
        res [ 0 ] = getContent ().substring ( temporaryCurrentIndex , temp ) ;
        temporaryCurrentIndex = temp + 1 ; 
        return res ;
    }
    
    /**
     * @return
     */
    protected String [] getNextElementBodyVariable () 
    {
        String res [] ;
        int counter = 0 ;
        while ( getContent ().charAt ( temporaryCurrentIndex + counter ) != getLanguageConfiguration ().getStatementEndDelimiter ().charAt ( 0 ) )
            counter ++ ;
        counter ++ ;
        res = new String [] { getContent ().substring ( temporaryCurrentIndex , temporaryCurrentIndex + counter ) } ;
        temporaryCurrentIndex += counter ;
        currentIndex = temporaryCurrentIndex ;
        return res ;
    }
    
    /**
     * @return
     */
    protected String getNextElementHead () 
    {
        String res ;
        int counter = 0 ;
        while ( getContent().charAt ( currentIndex + counter ) != ' ' &&
                getContent().charAt ( currentIndex + counter ) != getLanguageConfiguration ().getBlockStartDelimiter ().charAt ( 0 ) )
            counter ++ ;
        res = getContent().substring ( currentIndex , currentIndex + counter ) ;
        temporaryCurrentIndex = currentIndex + counter ;
        return res ;
    }
    
    /***/
    protected void reachOpenCurlyBrace () 
    {
        int counter ;
        counter = 0 ;
            while ( getContent ().charAt ( temporaryCurrentIndex + counter ) != getLanguageConfiguration ().getBlockStartDelimiter ().charAt ( 0 ) )
                counter ++ ;
        temporaryCurrentIndex += counter ;
    }
    
    /**
     * @return
     */
    protected int skipBlock () 
    {
        int counter ;
        int slack ;
        counter = 1 ;
        slack = 0 ;
        while ( getContent ().charAt ( temporaryCurrentIndex + counter ) != getLanguageConfiguration ().getBlockEndDelimiter ().charAt ( 0 ) || slack > 0 )
        {   
            if ( getContent ().charAt ( temporaryCurrentIndex + counter ) == getLanguageConfiguration ().getBlockStartDelimiter ().charAt ( 0 ) )
                slack ++ ;
            else
                if ( getContent ().charAt ( temporaryCurrentIndex + counter ) == getLanguageConfiguration ().getBlockEndDelimiter ().charAt ( 0 ) )
                    slack -- ;
                counter ++ ;
        }
        counter ++ ;
        return counter ;
    }
    
    /**
     * @return
     */
    protected int skipCondition () 
    {
        int counter ;
        int slack ;
        counter = 1 ;
        slack = 0 ;
        while ( getContent ().charAt ( temporaryCurrentIndex + counter ) != getLanguageConfiguration ().getConditionEndDelimiter ().charAt ( 0 ) || slack > 0 )
        {   
            if ( getContent ().charAt ( temporaryCurrentIndex + counter ) == getLanguageConfiguration ().getConditionStartDelimiter ().charAt ( 0 ) )
                slack ++ ;
            else
                if ( getContent ().charAt ( temporaryCurrentIndex + counter ) == getLanguageConfiguration ().getConditionEndDelimiter ().charAt ( 0 ) )
                    slack -- ;
                counter ++ ;
        }
        counter ++ ;
        return counter ;
    }
    
    /***/
    protected void trim () 
    {
        int counter ;
        counter = 0 ;
        while ( getContent ().charAt ( temporaryCurrentIndex + counter ) == " ".charAt ( 0 ) || 
                getContent ().charAt ( temporaryCurrentIndex + counter ) == "\n".charAt ( 0 ) ) 
        {
            counter ++ ;
        }
        temporaryCurrentIndex += counter ;
    }
    
}