/***/

package jcodeanalyzer.parser;

import java.util.Collections;
import java.util.List;
import jcodeanalyzer.help.LanguageConfiguration;
import jcodeanalyzer.model.Addition;
import jcodeanalyzer.model.Assignment;
import jcodeanalyzer.model.Division;
import jcodeanalyzer.model.IFunctionElement;
import jcodeanalyzer.model.Multiplication;
import jcodeanalyzer.model.Program;
import jcodeanalyzer.model.SimpleStatement;
import jcodeanalyzer.model.Subtraction;

/**
 *
 * @author Francesco Bertelli
 * @see AbstractParser
 */
public class SimpleStatementParser 
    extends AbstractParser
{

    // Instance Fields
    
    private Integer currentIndex ;
    
    private Integer temporaryCurrentIndex ;
    
    // Instance Methods
    
    /**
     * @param languageConfiguration 
     * @param content 
     * @param program
     */
    public SimpleStatementParser( LanguageConfiguration languageConfiguration , Program program , String content ) 
    {
        super ( languageConfiguration , program , content ) ;
    }
    
    /**
     * AS THE SUPER'S ONE.
     */
    @Override
    public List < IFunctionElement > parse () 
    {
        List < IFunctionElement > res ;
        String type ;
        IFunctionElement element ;
        int pos ;
        pos = getContent ().indexOf ( "+" );
        if ( pos != -1 )
            element = new Addition ( getContent () ) ;
        else
        {
            pos = getContent ().indexOf ( "-" ) ;
            if ( pos != -1 )
                element = new Subtraction ( getContent () ) ;
            else
            {
                pos = getContent ().indexOf ( "*" ) ;
                if ( pos != -1 )
                    element = new Multiplication ( getContent () ) ;
                else
                {
                    pos = getContent ().indexOf ( "/" ) ;
                    if ( pos != -1 )
                        element = new Division ( getContent () ) ;
                    else
                        element = new Assignment ( getContent () ) ;
                }
            }
        }
        res = Collections.singletonList ( element ) ;
        return res ;
    }
    
    /**
     * @return
     */
    private String getNextElementHead () 
    {
        String res ;
        int counter = 0 ;
        while ( getContent().charAt ( currentIndex + counter ) != " ".charAt ( 0 ) &&
                getContent().charAt ( currentIndex + counter ) != getLanguageConfiguration ().getBlockStartDelimiter ().charAt ( 0 ) )
            counter ++ ;
        res = getContent().substring ( currentIndex , currentIndex + counter ) ;
        temporaryCurrentIndex = currentIndex + counter ;
        return res ;
    }
    
}
