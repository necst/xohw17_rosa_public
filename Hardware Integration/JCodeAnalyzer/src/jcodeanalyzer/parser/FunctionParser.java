/***/
package jcodeanalyzer.parser;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jcodeanalyzer.help.LanguageConfiguration;
import jcodeanalyzer.model.Function;
import jcodeanalyzer.model.IFunctionElement;
import jcodeanalyzer.model.Program;
import jcodeanalyzer.model.Variable;

/**
 *
 * @author Francesco Bertelli
 * @see AbstractParser
 * @see String
 */
public class FunctionParser 
    extends AbstractParser 
{
    
    // Static Fields
    
    /***/
    private static final Logger LOGGER = Logger.getLogger ( FunctionParser.class.getName () ) ;
 
    // Instance Methods
    
    /**
     * @param languageConfiguration
     * @param content
     * @param program 
     */
    public FunctionParser ( LanguageConfiguration languageConfiguration , Program program , String content )
    {
        super ( languageConfiguration , program , content ) ;
    }

    /**
     * AS THE SUPER'S ONE.
     */
    @Override
    public List < Function > parse () 
    {
        List < IFunctionElement > body ;
        List < Variable > prototype ;
        Function function ;
        FunctionBodyParser functionBodyParser ;
        FunctionPrototypeParser functionPrototypeParser ;
        String functionPrototype ;
        String functionBody ;
        int functionBodyStartIndex ;
        int functionBodyEndIndex ;
        functionBodyStartIndex = getContent().indexOf ( getLanguageConfiguration ().getFunctionStartDelimiter () ) ;
        functionBodyEndIndex = getContent().lastIndexOf ( getLanguageConfiguration ().getFunctionEndDelimiter () ) ;
        functionPrototype = getContent().substring ( 0 , functionBodyStartIndex ) ;
        LOGGER.log ( Level.INFO , "Function Prototype Found : " + functionPrototype + "\n" ) ;
        functionBody = getContent().substring ( functionBodyStartIndex + 1 , functionBodyEndIndex ) ;
        LOGGER.log ( Level.INFO , "Function Body Found : " + functionBody + "\n" ) ;
        functionBodyParser = new FunctionBodyParser ( getLanguageConfiguration () , getProgram () , functionBody.trim () ) ;
        functionPrototypeParser = new FunctionPrototypeParser ( getLanguageConfiguration () , getProgram () , functionPrototype.trim () ) ;
        LOGGER.log ( Level.INFO , "Start Parsing Function Prototype " + functionPrototype + "\n" ) ;
        prototype = functionPrototypeParser.parse () ;
        LOGGER.log ( Level.INFO , "End Parsing Function Prototype" + "\n" ) ;
        LOGGER.log ( Level.INFO , "Start Parsing Function Body " + functionBody + "\n" ) ;
        body = functionBodyParser.parse () ;
        LOGGER.log ( Level.INFO , "End Parsing Function Body" + "\n" ) ;
        function = new Function ( functionPrototype.split ( " " ) [ 1 ].trim () , functionPrototype.split ( " " ) [ 0 ].trim () ) ;
        //prototype.stream ().forEach ( v -> { function.addPrototypeVariable ( v ) ; } ) ;
        body.stream ().forEach ( e -> { function.addStatement ( e ) ; } ) ;
        return Collections.singletonList ( function ) ;
    }

}
