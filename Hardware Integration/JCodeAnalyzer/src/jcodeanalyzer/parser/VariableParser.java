/***/
package jcodeanalyzer.parser;

import java.util.Collections;
import java.util.List;
import jcodeanalyzer.help.LanguageConfiguration;
import jcodeanalyzer.model.Program;
import jcodeanalyzer.model.Variable;

/**
 *
 * @author Francesco Bertelli
 * @see AbstractParser
 */
public class VariableParser 
    extends AbstractParser
{

    // Instance Fields
    
    /***/
    private Integer currentIndex ;
    
    /**
     * @param languageConfiguration 
     * @param content 
     * @param program 
     */
    public VariableParser ( LanguageConfiguration languageConfiguration , Program program , String content ) 
    {
        super ( languageConfiguration , program , content ) ;
        currentIndex = 0 ;
    }

    /**
     * AS THE SUPER'S ONE.
     */
    @Override
    public List < Variable > parse () 
    {
        String type ;
        String name ;
        String multiplicity ;
        Variable variable ;
        boolean isArray ;
        int firstSpaceIndex ;
        int secondSpaceIndex ;
        firstSpaceIndex = getContent ().indexOf ( " " ) ;
        type = getContent().substring ( 0 , firstSpaceIndex ) ;
        currentIndex = firstSpaceIndex ;
        while ( getContent ().charAt ( currentIndex ) == " ".charAt ( 0 ) )
            currentIndex ++ ;
        secondSpaceIndex = getContent ().indexOf ( " " , currentIndex ) ;
        name = getContent ().substring ( currentIndex , secondSpaceIndex ) ;
        if ( ! getContent ().contains ( getLanguageConfiguration ().getArrayOpenDelimiter() ) )
        {
            multiplicity = "1" ;
            isArray = false ;
        }
        else
        {
            multiplicity = getContent ().substring ( getContent().indexOf ( getLanguageConfiguration ().getArrayOpenDelimiter() ) + 1 , getContent ().indexOf ( getLanguageConfiguration ().getArrayCloseDelimiter() ) ) ;
            isArray = true ;
        }
        variable = new Variable ( isArray , name , type , multiplicity , null ) ;
        return Collections.singletonList ( variable ) ;
    }
    
}
