/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcodeanalyzer.fpga;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author parallels
 */
public class VivadoHLSReportParser 
{

    // Instance Methods
    
    /***/
    public VivadoHLSReportParser () {}
    
    public VivadoHLSReport parse ( String filePath ) 
    {
        VivadoHLSReport res ;
        VivadoHLSResourceReport usedResources ;
        VivadoHLSResourceReport totalResources ;
        File fXmlFile = new File(filePath);
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try 
        {
            Document doc ;
            DocumentBuilder builder;
            NodeList list ;
            XPath xpath ;
            XPathExpression expr ;
            XPathFactory xPathfactory ;
           
            builder = dbFactory.newDocumentBuilder();
            doc = builder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            xPathfactory = XPathFactory.newInstance();
            xpath = xPathfactory.newXPath();
            expr = xpath.compile("/profile/AreaEstimates/Resources/*" ) ;
            list = ( NodeList ) expr.evaluate(doc,XPathConstants.NODESET ) ;
            usedResources = generate ( list ) ;
            System.out.println(usedResources);
            expr = xpath.compile("/profile/AreaEstimates/AvailableResources/*" ) ;
            list = ( NodeList ) expr.evaluate(doc,XPathConstants.NODESET ) ;
            totalResources = generate ( list ) ;
            res = new VivadoHLSReport ( usedResources , totalResources ) ;
            System.out.println(res);
            
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(VivadoHLSReportParser.class.getName()).log(Level.SEVERE, null, ex);
            res=null;
        } catch (SAXException | IOException | XPathExpressionException ex) {
            Logger.getLogger(VivadoHLSReportParser.class.getName()).log(Level.SEVERE, null, ex);
            res=null;
        }
        return res ;
    }
    
    private VivadoHLSResourceReport generate ( NodeList list )
    {
        VivadoHLSResourceReport res ;
        int dsp ;
        int ff ;
        int lut ;
        int bram ;
        dsp = 0 ;
        ff = 0 ;
        lut = 0 ;
        bram = 0 ;
        for ( int i = 0 ; i < list.getLength () ; i ++ )
            switch ( list.item(i).getNodeName().toUpperCase().trim() )
            {
                case "DSP48" :
                    dsp = Integer.parseInt ( list.item ( i ).getTextContent ().trim () ) ;
                break ;
                case "FF" :
                    ff = Integer.parseInt ( list.item ( i ).getTextContent ().trim () ) ;
                break ;
                case "LUT" :
                    lut = Integer.parseInt ( list.item ( i ).getTextContent ().trim () ) ;
                break ;
                case "BRAM_18K" :
                    bram = Integer.parseInt ( list.item ( i ).getTextContent ().trim () ) ;
                break ;
                default :
                    throw new IllegalArgumentException () ;
            }
        res = new VivadoHLSResourceReport ( dsp , ff , lut , bram ) ;
        return res ;
    }
    
}
