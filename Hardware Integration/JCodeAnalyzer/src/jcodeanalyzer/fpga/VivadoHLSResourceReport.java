/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcodeanalyzer.fpga;

/**
 *
 * @author parallels
 */
public class VivadoHLSResourceReport 
{

    private int dsp ;
    
    private int ff ;
    
    private int lut ;
    
    private int bram ;
    
    public VivadoHLSResourceReport ( int dsp , int ff , int lut , int bram ) 
    {
        if ( dsp >= 0 && ff >= 0 && lut >= 0 && bram >= 0 )
        {
            this.dsp = dsp ;
            this.bram = bram ;
            this.ff = ff ;
            this.lut = lut ;
        }
        else
            throw new IllegalArgumentException () ;
    }

    @Override
    public String toString () 
    {
        String res ;
        StringBuffer s ;
        s = new StringBuffer ( 4 ) ;
        s.append ( "DSP : " ) ;
        s.append ( dsp ) ;
        s.append ( "\n" ) ;
        s.append ( "FF : " ) ;
        s.append ( ff ) ;
        s.append ( "\n" ) ;
        s.append ( "LUT :" ) ;
        s.append ( lut ) ;
        s.append ( "\n" ) ;
        s.append ( "BRAM :" ) ;
        s.append ( bram ) ;
        s.append ( "\n" ) ;
        res = s.toString() ;
        return res ;
    }
    
    /**
     * @return the dsp
     */
    public int getDsp() {
        return dsp;
    }

    /**
     * @return the ff
     */
    public int getFf() {
        return ff;
    }

    /**
     * @return the lut
     */
    public int getLut() {
        return lut;
    }

    /**
     * @return the bram
     */
    public int getBram() {
        return bram;
    }
    
    
    
}
