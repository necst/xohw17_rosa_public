/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcodeanalyzer.fpga;

/**
 *
 * @author parallels
 */
public class VivadoHLSReport 
{

    // Instance Fields
    
    private VivadoHLSResourceReport usedResources ;
    
    private VivadoHLSResourceReport totalResources ;
    
    // Instance Methods
    
    public VivadoHLSReport ( VivadoHLSResourceReport usedResources , VivadoHLSResourceReport totalResources ) 
    {
        if ( usedResources != null && totalResources != null )
        {
            this.usedResources = usedResources ;
            this.totalResources = totalResources ;
        }
        else
            throw new IllegalArgumentException () ;
    }
    
    @Override
    public String toString () 
    {
        String res ;
        StringBuffer s ;
        s = new StringBuffer ( 10 ) ;
        s.append ( "Used Resources : \n" ) ;
        s.append ( usedResources.toString () ) ;
        s.append ( "Total Resources : \n" ) ;
        s.append ( totalResources.toString () ) ;
        res = s.toString () ;
        return res ;
    }
    
}
