# ROSA

### Roofline OpenCL SDAccel Project

Folder Structure

* The Static Analyzer folder contains an initial version of a Static Code Analyzer written in Python used to calculate the Operational Intensity needed to generate the Roofline Model.

* The Hardware Integration folder contains a different Static Code Analyzer written in Java that is the base for the integration with
HLS tools.

Note

All the material here is rapidly changing since the project is still under development.

