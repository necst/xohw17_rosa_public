import copy
import sys
import os
import shutil

import networkx as nx
import matplotlib.pyplot as plt

DEBUG = False
if len(sys.argv) == 3 and sys.argv[2] == "-d":
    DEBUG = True

class treeNode:
    inCoLoop = None
    children = []

    def __str__(self):
        return "Node containing {" + str(self.inCoLoop) + "} with children {" + str(self.children) + "}"
    def __repr__(self):
        return "Node containing {" + str(self.inCoLoop) + "} with children {" + str(self.children) + "}"
    
    def addChild(self, obj):
        tempNode = treeNode()
        tempNode.inCoLoop = obj
        tempNode.children = []
        self.children.append(copy.copy(tempNode))

class basicBlock:
    label = ""
    pred = []
    succ = []
    instructions = []
    isBranching = False

    def __str__(self):
        ostr = "Label: " + str(self.label) + ", pred = " + str(self.pred) + ", instructions = " + str(self.instructions)
        return ostr

class function:
    name = ""
    blocks = [] #A list of basic blocks
    loops = []
    coLoops = []
    graph = None

    def __str__(self):
        ostr = "Function " + self.name + "\n"
        for bl in self.blocks:
            ostr += "> " + str(bl.label) + " - pred = " + str(bl.pred) + "\n"
        ostr += "Loops present: " + str(self.loops) + "\n"
        return(ostr)

class branch:
    label = ""
    splice = [] #The branching instruction
    condition = "" #The register that is checked
    trueLabel = "" #The label where it branches if true
    falseLabel = "" #The label where it branches if false
    #lefthand = 0
    #righthand = 0
    function = None

    def __str__(self):
        ostr = "On label " + str(self.label) + ", branch on [" + self.condition + "] to " + str(self.trueLabel) + " and " + str(self.falseLabel)
        return ostr
    def __repr__(self):
        ostr = "On label " + str(self.label) + ", branch on [" + self.condition + "] to " + str(self.trueLabel) + " and " + str(self.falseLabel)
        return ostr

class loop:
    # Loops have more than one branch. yaaay...?
    branches = []
    function = None
    # Should it have blocks rather than labels? Possibly.
    labels = []
    comparisons = []
    # In the future, I must introduce parameters here. At the moment, I am only working on a static version.
    increment = ""
    arithmeticInsts = 0
    runTimesEquation = ""
    doesComparisonChange = False

    def __str__(self):
        return("Labels " + str(self.labels))
    def __repr__(self):
        return("Labels " + str(self.labels))

class collapsedLoop:
    loops = []
    function = None
    commonLabels = []
    parallelPaths = []
    parallelUnion = []
    
    branches = []
    exitBranches = []
    tempBranches = []
    criticalBranch = None

    nestedLoops = []
    isNested = False

    condition = ""
    leftHandLiteral = ""
    leftHandInit = ""
    leftHandIncrement = ""
    rightHandInit = ""
    rightHandLiteral = ""

    def __str__(self):
        return("In function " + self.function.name + ", Common Labels: " + str(self.commonLabels) + ", Parallel Paths: " + str(self.parallelPaths) + ", united in " + str(self.parallelUnion) + ", nested = " + str(self.isNested) + ", nested loops are {" + str(self.nestedLoops) + "}, rightHand starts at " + str(self.rightHandInit) + ", leftHand starts at " + str(self.leftHandInit))
    def __repr__(self):
        return("In function " + self.function.name + ", Common Labels: " + str(self.commonLabels) + ", Parallel Paths: " + str(self.parallelPaths) + ", united in " + str(self.parallelUnion) + ", nested = " + str(self.isNested) + ", nested loops are {" + str(self.nestedLoops) + "}, rightHand starts at " + str(self.rightHandInit) + ", leftHand starts at " + str(self.leftHandInit))

    def getAllLabels(self):
        return list(set(self.commonLabels).union(set(self.parallelUnion)))

    def getExitBranchFromLabel(self, lbl):
        for eB in self.exitBranches:
            if eB.label == lbl:
                return eB

# It would be theoretically better to have this as methods of the various classes
def getBlockFromLabel(label, blocks):
    for bl in blocks:
        if bl.label == label:
            return bl
    return 0

# 
def distToZero(fun, lbl):
    G = fun.graph
    d = 0
    foundZero = False
    frontier = G.predecessors(lbl)
    while foundZero == False:
        d += 1
        if "0" in frontier:
            foundZero = True
        else:
            reList = []
            for n in frontier:
                reList = list(set(reList).union(set(G.predecessors(n))))
            frontier = reList
    return d

def addChildren(loopNode):
    for nL in loopNode.inCoLoop.nestedLoops:
        loopNode.addChild(nL)
    for c in loopNode.children:
        addChildren(c)

def treeTrace(tNode, level=0):
    # Can only be None if it's the base node.
    outstring = ""
    if (tNode.inCoLoop is not None):
        traceIncrement(tNode.inCoLoop)
        if tNode.inCoLoop.rightHandInit == tNode.inCoLoop.leftHandInit:
            print("WARNING! Loop apparently never iterates. Suspected after-initialization increment of the righthand member " + tNode.inCoLoop.rightHandLiteral)
            print(str(tNode.inCoLoop))
        
        outstring += "\\frac{\\text{" + str(tNode.inCoLoop.rightHandInit)
        if tNode.inCoLoop.leftHandInit != '0':
            outstring = outstring + "-" + str(tNode.inCoLoop.leftHandInit)
        outstring = outstring + "}}{" + tNode.inCoLoop.leftHandIncrement + "}"
        # This gives me slight inconsistencies. Possibly making this mandatory here and remove it in the else case might help.
        cal, mem, ari = traceOperations(tNode.inCoLoop)
        outstring += "[" + str(mem) + " \\text{ memory, }" + str(ari) + "\\text{ arithmetic}"
        if len(cal) > 0:
            # Maybe count calls?
            outstring += "\\text{, calls to }"
            for c in cal:
                outstring += c + ", "
            outstring = outstring[:-2]
        outstring += "]"
        if tNode.children != []:
            for c in tNode.children:
                outstring += "\\end{align*}\\vspace{-2em}\\\\\\begin{align*}" + "|\\hspace{.6em}"*level + treeTrace(c, level+1)
    else:
        outstring = "\\begin{align*}"
        for c in tNode.children:
            outstring = outstring + treeTrace(c, level+1) + "\\end{align*}\\vspace{-2em}\\\\\\begin{align*}"
        outstring = outstring[:-15]

    #outstring = outstring.replace("%", "\%")
    return outstring

def traceOpsFromInstr(instr, func):
    memoryCount = 0
    arithmCount = 0
    callList = []
    for l in instr:
        splice = l.split(' ')
        if len(splice) > 2:
            #if splice[0] == "store" or splice[2] == "load" or splice[2] == "alloca" or splice[2] == "getelementptr":
            if splice[2] == "alloca" or splice[2] == "getelementptr":
                memoryCount += 1
            if splice[2] in ["add","sub","mul","udiv","sdiv","fdiv","urem","srem","frem"]:
                arithmCount += 1
            if "call" in splice:
                i = 0
                while splice[i][0] != '@':
                    i += 1
                # The ugly concatenation of text is needed at least until I refactor all the outputs coherently.
                ostring = "\\text{" + splice[i][1:].split("(")[0].translate(None, "@") + "}"
                if "getelementptr" in splice:
                    while splice[i][1] != "[":
                        i += 1
                    ostring += "\\text{(ptr(" + str(splice[i].translate(None, "([")) + "*" + str(splice[i+2].translate(None, "],") + "))}")
                else:
                    ostring += "("
                    while splice[i][-1] != ")":
                        i += 1
                        if splice[i][0] == "%" or splice[i].translate(None, ",)").isdigit():
                            ostring += "\\text{" + str(traceVariable(splice[i].translate(None, ",)"), func)) + "}, "
                            # Here I could also traceback to a load instruction
                    if ostring[-3:] == "}, ":
                        ostring = ostring[:-2]
                    ostring += ")"
                callList.append(ostring)
    return callList, memoryCount, arithmCount


def traceOperations(coLoop):
    bigList = []
    secondList = []
    for lab in coLoop.commonLabels:
        bigList += getBlockFromLabel(lab, coLoop.function.blocks).instructions
    for lab in coLoop.parallelUnion:
        secondList += getBlockFromLabel(lab, coLoop.function.blocks).instructions

    calls, memo, arit = traceOpsFromInstr(bigList, coLoop.function)
    adcalls, admemo, adarit = traceOpsFromInstr(secondList, coLoop.function)
    if len(coLoop.parallelPaths) > 0:
        memo += admemo/len(coLoop.parallelPaths)
        arit += adarit/len(coLoop.parallelPaths)
    calls += adcalls
    # Here I track, again, every pPath through a dictionary, then throw an average and sayonara. To make it good, I should associate each pPath's increment to its operations.
    return calls, memo, arit

def traceIncrement(coLoop):
    leftHandRegister = coLoop.criticalBranch.condition.split(' ')[-2].translate(None, ",")
    tracker = leftHandRegister
    # Possibly one of the weirdest ways to do this, shows why planning inCoLoops is wasted time
    flag = False
    while flag == False:
        for l in (getBlockFromLabel(coLoop.criticalBranch.label, coLoop.function.blocks)).instructions:
            splice = l.split(' ')
            #print(splice)
            # BIG SURPRISE! It's not always a load. Gotta need a traceback?
            if splice[0] == tracker:
                if splice[2] in ["sitofp","bitcast","sitofp","uitofp","fptosi","fptoui"]:
                    tracker = splice[4]
                elif splice[2] == "load":
                    coLoop.leftHandLiteral = splice[5].translate(None, ",")
                    flag = True
        else: # In case it never found anything. Hopefully it wou't freak things up.
            flag=True
    if DEBUG == True:
        print(coLoop.function.name + " - " + leftHandRegister + " - " + coLoop.leftHandLiteral)
    # Here I should set up a list of labels generated through a comprehension from commonLabels unito a parallelUnion. Otherwise, I'll trace the commons first, the parallels later (care, since you remember how parallel paths can change different trackers (mergesort-like))
    commonList = [getBlockFromLabel(lbl, coLoop.function.blocks) for lbl in list(set(coLoop.commonLabels).union(set(coLoop.parallelUnion)))]
    # I can use in and a splice to check if a line contains the right literal
    loadFlag = False
    storeFlag = False
    registerNumeral = None
    mainMod = ""
    paraDict = {}
    for pP in coLoop.parallelPaths:
        paraDict[str(pP)] = ""
    for blo in commonList:
        for line in blo.instructions:
            splice = line.split(' ')
            splice = [s.translate(None, ",") for s in splice]
            if coLoop.leftHandLiteral in splice:
                if splice[2] == "load":
                    loadFlag = True
                    registerNumeral = splice[0]
                    if DEBUG == True:
                        print("Started tracking " + coLoop.leftHandLiteral + ", loaded as " + registerNumeral)
                    # TODO I also must add the remainder instructions and the dynamic file loading.
                    for inBlock in commonList:
                        for inLine in inBlock.instructions:
                            inSplice = inLine.split(' ')
                            inSplice = [s2.translate(None, ",") for s2 in inSplice]
                            if registerNumeral in inSplice:
                                addendum = ""
                                if inSplice[2] == "add":
                                    registerNumeral = inSplice[0]
                                    addendum = addendum + "+" + str(traceVariable(str(inSplice[-1]), coLoop.function))
                                    if DEBUG == True:
                                        print("Tracking " + registerNumeral + ", modifier increased to " + addendum + " in " + inLine)
                                elif inSplice[2] == "sub":
                                    registerNumeral = inSplice[0]
                                    addendum = addendum + "-" + str(traceVariable(str(inSplice[-1]), coLoop.function))
                                    if DEBUG == True:
                                        print("Tracking " + registerNumeral + ", modifier increased to " + addendum + " in " + inLine)
                                elif inSplice[2] == "mul" or inSplice[2] == "fmul":
                                    registerNumeral = inSplice[0]
                                    addendum = "(" + addendum + ")*" + str(traceVariable(str(inSplice[-1]), coLoop.function))
                                    if DEBUG == True:
                                        print("Tracking " + registerNumeral + ", modifier increased to " + addendum + " in " + inLine)
                                elif inSplice[0] == "store" and registerNumeral in inSplice:
                                    storeFlag = True
                                    if DEBUG == True:
                                       print("Found a store in " + inLine)
                                if inBlock.label in coLoop.parallelUnion:
                                    for p in coLoop.parallelPaths:
                                        if inBlock.label in p:
                                            paraDict[str(p)] += addendum
                                else:
                                    mainMod += addendum
                    if storeFlag == False:
                        loadFlag = False
                        if DEBUG == True:
                            print("Stopped tracking " + registerNumeral)
                        addendum = ""
                        mainMod = ""
                        for key in paraDict:
                            paraDict[key] = ""
    # Is it possible that here I start tracing from a load and get to the last store? Or that I miss the store? Should I do one trip for every load? Likely so.

    if loadFlag == True and storeFlag == True:
        fullModifier = mainMod
        if fullModifier != "":
            if fullModifier[0] == "+":
                fullModifier = fullModifier[1:]
            fullModifier = fullModifier.replace("()", "(" + coLoop.leftHandLiteral + ")")
            for key in paraDict:
                paraDict[key] = paraDict[key].replace("()", "(" + coLoop.leftHandLiteral + ")")
            if paraDict != {}:
                flag = False
                for key in paraDict:
                    if paraDict[key] != "":
                        flag = True
                if flag == True:
                    fullModifier += "+"
        if paraDict != {}:
            tempMod = "\\frac{"
            toWrite = False
            for key in paraDict:
                if paraDict[key] != "":
                    val = paraDict[key]
                    if (val[0]) == "+":
                        val = val[1:]
                    tempMod += val + "+"
                    toWrite = True
            if toWrite == True:
                while tempMod[-1] == "+":
                    tempMod = tempMod[:-1]
                tempMod += "}{" + str(len(paraDict)) + "}"
                fullModifier += tempMod
        if DEBUG == True:
            print (coLoop.function.name + " - in the current loop " + coLoop.leftHandLiteral + " changes by " + fullModifier)
        coLoop.leftHandIncrement = fullModifier
        # I will assume only one store per loop. It's a pity that there loads aren't guaranteed to be in the same block.
        # What would happen if I had a load in the commonLabels and a store in the parallelPaths? Should address it.

    # The increments to be traced are only those between a load and a store (otherwise I'd see a fake increment from matrix indexing like M[i*W+j]. I should also warn, when there are no increments but there are getelementptr instructions, that loading is not traceable (for example the loop running while dirMatrix != STOP)

def traceVariable(label, func, forced=False):
    if DEBUG == True:
        print("Looking for " + str(label) + " in " + func.name)
    if label.isdigit():
        return int(label)
    if not (any(char.isdigit() for char in label)) and not forced:
        return str(label)
    # Weird cases that sometimes happens with some flags and pointers
    if label == "":
        return("NULL")
    if label[0] == '@':
        return(label)
    toEval = ""
    blocks = func.blocks
    for b in blocks:
        for i in b.instructions:
            # This fires if the mark is the one we're looking for
            if i.split(' ')[0] == label.translate(None, ","):
                toEval = i
    # toEval empty means a parameter is passed. Therefore, parametrism will have to start. If I get the formula of the atomic functions, then it will all be a game of reconstruction.
    if toEval == '':
        if label.translate(None, ",").isdigit():
            return label.translate(None, ",")
        else:
            return  label.translate(None, ",")
    splice = toEval.split(' ')
    # Idea for refactoring: Set a translate (None, ",") here and avoi doing it over and over

    """
    List of assembly instructions considered so far:
    - icmp
    - fcmp
    - load
    - store
    - alloca
    - sext
    - trunc
    - add
    - sub
    - mul
    - call
    - bitcast
    """
    # TODO include rem, the remainder. How? Somehow.

    # Load gets a variable. Also LOL python has no switch
    instr = splice[2]
    if instr == "load":
        return traceVariable(splice[5].translate(None, ","), func, forced)
    # registers are loaded from variables, variables are either loaded from numbers or something else

    # TODO alloca implies that store is called ONLY when everything is static. With a dynamic program there are functions that can be called (such as getline). CAREFUL.
    elif instr == "alloca":
        for b in blocks:
            for i in b.instructions:
                innerSplice = i.split(' ')
                if innerSplice[0] == "store" and innerSplice[4].translate(None, ",") == label:
                    if DEBUG == True:
                        print(i)
                    return traceVariable(innerSplice[2], func, forced)
                elif innerSplice[0] == "store" and innerSplice[2] == "getelementptr" and innerSplice[-3].translate(None, ",") == label:
                    if DEBUG == True:
                        print(i)
                    return "ptr(" + str(innerSplice[4].translate(None, "([")) + "*" + str(innerSplice[6].translate(None, "],")) + ")"
        if len(splice) > 6:
            for i in range(len(splice)):
                if splice[i][0] == '%' and splice[i] != label:
                    return "ptr(" + traceVariable(splice[i].translate(None, ","), func, forced) + "*" + splice[i-1] + ")"
    # Trunc and sext have the same syntax and change nothing. Not much, at least.

    elif instr == "sext" or splice[2] == "trunc" or instr == "zext":
        return traceVariable(splice[4].translate(None, ","), func, forced)
    
    elif instr == "add":
        l = 4
        while(splice[l][0] != "%"):
            l += 1
        left = traceVariable(splice[l].translate(None, ","), func, forced)
        right = traceVariable(splice[l+1].translate(None, ","), func, forced)
        if (type(left) == int and type(right) == int):
            return(left+right)
        else:
            return(str(left) + "+" + str(right))
    
    elif instr == "sub":
        l = 4
        while(splice[l][0] != "%"):
            l += 1
        left = traceVariable(splice[l].translate(None, ","), func, forced)
        right = traceVariable(splice[l+1].translate(None, ","), func, forced)
        if (type(left) == int and type(right) == int):
            return(left-right)
        else:
            return(str(left) + "-" + str(right))
    
    elif instr == "mul" or instr == "fmul":
        l = 4
        while(splice[l][0] != "%" and not splice[l].translate(None, "e+-,.").isdigit()):
            l += 1
        left = traceVariable(splice[l].translate(None, ","), func, forced)
        right = traceVariable(splice[l+1].translate(None, ","), func, forced)
        if (type(left) == int and type(right) == int):
            return(left*right)
        else:
            return(str(left) + "*" + str(right))

    elif instr == "div" or instr == "sdiv" or instr == "udiv" or instr == "fdiv":
        l = 4
        while(splice[l][0] != "%"):
            l += 1
        left = traceVariable(splice[l].translate(None, ","), func, forced)
        right = traceVariable(splice[l+1].translate(None, ","), func, forced)
        if (type(left) == int and type(right) == int):
            return(left/right)
        else:
            return(str(left) + "/" + str(right))

    # Calls to void functions are ignorable here, since they can't be used for traceback.
    elif instr == "call":
        # Remove parentheses and unneeded types
        recomposition = ""
        for i in splice:
            recomposition = recomposition + i + " "
        params = recomposition.split('(')[1].split(')')[0].translate(None, ",").split(' ')
        # Cut keywords
        i = 4
        while splice[i][0] != "@":
            i += 1
        outString = (splice[i].split('(')[0].translate(None, ",@") + "(")
        for i in range(1, len(params), 2):
            outString = outString + str(traceVariable(params[i], func, forced)) + ", "
        # [:-2] because I added ", " at every end
        outString = outString[:-2] + ")"
        return outString

    elif instr == "bitcast" or instr == "sitofp" or instr == "uitofp" or instr == "fptosi" or instr == "fptoui":
        return traceVariable(splice[4], func, forced)
    
    elif instr == "select":
        # Here should be a traceCondition for the ternary operator
        return("(" + str(traceVariable(splice[-3].translate(None, ","), func, forced)) + "/" + str(traceVariable(splice[-1].translate(None, ","), func, forced)) + ")")

    elif instr == "getelementptr":
        ostr = ""
        counter = 3
        if splice[counter] == "inbounds":
            counter += 2;
        ostr += str(traceVariable(splice[counter+1], func, forced))
        counter += 2
        while counter < len(splice):
            ostr += "[" + str(traceVariable(splice[counter+1].translate(None, ","), func, forced)) + "]"
            counter += 2
        print(ostr)
        return ostr
        '''
        if splice[-4][-1] == '*':  # Idk how to get it otherwise
            return(str(traceVariable(splice[-3].translate(None, ","), func, forced)) + "[" + str(traceVariable(splice[-1].translate(None, ","), func, forced)) + "]")
        
        else:
            return("[" + toEval + ": not yet implemented]")
        '''
    else:
        return ("[" + instr + " is not yet implemented]")

def traceCondition(bra, coLo):
    # How on Earth can't it work properly just with the coLoop when the conditions are the same? No clue, I blame black magic.
    if bra.condition.split(' ')[2] == "icmp" or bra.condition.split(' ')[2] == "fcmp":
        coLo.leftHandInit = traceVariable(bra.condition.split(' ')[-2].translate(None, ","), coLo.function, forced=True) 
        if DEBUG == True:
            print(bra.function.name + " - for " + bra.condition + " I traced back the lefthand to " +str(coLo.leftHandInit))
        coLo.rightHandInit = traceVariable(bra.condition.split(' ')[-1], coLo.function, forced=True)
        if DEBUG == True:
            print(bra.function.name + " - for " + bra.condition + " I traced back the righthand to " + str(coLo.rightHandInit))
        #print(coLo)
        '''
        # phis are sometimes inconsequential, but they can (getBestMatch %184) be used to compare loops. Hard to trace them, as they have edges incoming from both of the labels. Best I can do is find the condition to stay in the loop.
    elif bra.condition.split(' ')[2] == "phi":
        firstEdge = (str(bra.condition.split(' ')[6].translate(None, "%,")), str(bra.label))
        secondEdge = (str(bra.condition.split(' ')[10].translate(None, "%,")), str(bra.label))
        # I don't know how to check how it's executed. That's because, apparently, both labels are in the loops and the loop only gives nodes, not edges.
        # TODO: If it worked earlier, it worked badly. Do things properly, write a diagram where you mark the possible ways and find a way to turn this into a nice prettly little branch.
        # GAUDEAMUS IGITUR I can remove all the phis with a simplifycfg pass. ALL HAIL LLVM.
        # I can always create a new branch and populate it with the elements I want.

        bra.condition = getBlockFromLabel(newLabel, bra.function.blocks).instructions[-2]
        traceCondition(bra, coLo)
        '''

dump = []
blocks = []
functions = []

#Another thing to do is to pack this into a nice main
if len(sys.argv) < 2 or len(sys.argv) > 3:
    print("Wrong number of arguments! Please run this as " + str(sys.argv[0]) + " + filename and optional -d for debug")
    sys.exit()

if (sys.argv[1][-3:]) != ".ll":
    print("Wrong file format! Please use a .ll file")
    sys.exit()

with open(sys.argv[1]) as f:
    dump = f.readlines()

# Gotta have ASCII art
os.system("figlet ROSA")

newblock = basicBlock()
newfunc = function()

# TODO IMPORTANT TODO IMPORTANT TODO
#
# Calls have to be tracked, ffs. Especially if they are inside a loop.

print("Setting up function structures")
for line in dump:
    if line[0:6] == "define":
        #Functions are pushed by a '}'
        newfunc.blocks = []
        newblock.instructions = []
        newblock.instructions.append(line.strip())
    # Critedges should be the only things to appear beginning with a '.'
    elif line[0:9] == "; <label>" or line[0] == ".":
        newblock.instructions = []
        newblock.instructions.append(line.strip())
    #Inside the label there is an indent, this should protect me from preprocessor stuff.
    elif line[0:2] == "  ":
        newblock.instructions.append(line.strip())
    #Terminating on empty string
    elif line.strip() == "":
        #Redundant but clearer
        if newblock.instructions != []:
            newfunc.blocks.append(copy.deepcopy(newblock))
            #Otherwise I risk duplicating the last string over and over and over.
        newblock.blocks=[]
    #A closed curly means a finished function
    elif line.strip() == "}":
        newfunc.blocks.append(copy.deepcopy(newblock))
        functions.append(copy.deepcopy(newfunc))

#First we set up properly the function values. Then we draw.
for fu in functions:
    for bl in fu.blocks:
        ix = bl.instructions[0]
        #First label of a functions is zero
        if ix[0:6] == "define":
            fu.name = (ix.split(' ')[2][1:]).split('(')[0]
            bl.label = '0'
        else:
            #The number at the end. Finding the pred will be done later. Or I will create links from the branches.
            splice = ix.split(' ')
            if splice[1][0:7] == "<label>":
                bl.label = str(splice[1][8:])
            elif splice[0][0] == ".":
                bl.label = str(splice[0].translate(None, ":"))
            #Iterator for the last element.
            i = len(splice)-1
            tmppred = []
            while splice[i] != '=':
                # If it's a label
                if splice[i][0] == '%':
                    pred = splice[i].translate(None, "%,")
                    tmppred.append(pred)
                i-=1
            bl.pred = tmppred

for fu in functions:
    for bl in fu.blocks:
        tempsucc = []
        for tgt in fu.blocks:
            for lbl in tgt.pred:
                if lbl == bl.label:
                    tempsucc.append(tgt.label)
        bl.succ = tempsucc

#for fu in functions:
#    print(fu)

graphs = []
#nodes = []
#edges = []

fuGraph=nx.DiGraph()

print("Building function graphs")
for fu in functions:
    for bl in fu.blocks:
        nodedesc = bl.label
        fuGraph.add_node(nodedesc)
        for pr in bl.pred:
            predesc = pr
            fuGraph.add_edge(predesc, nodedesc)
    graphs.append(copy.deepcopy(fuGraph))
    fuGraph.clear()

#Graphs and functions are in the same order
for i in range(len(graphs)):
    nx.draw(graphs[i], with_labels=True)
    progname = sys.argv[1][:-3]
    if not os.path.exists(progname + "_graphs"):
            os.makedirs(progname + "_graphs")
    
    #translate to remove the "../", likely better to split('/')
    dest = progname + "_graphs/" + progname.split("/")[-1] + "_" + functions[i].name + ".png"
    plt.savefig(dest)
    plt.clf()

#So now I have the graphs and I have the functions, I need to combine them. Linking a graph to a function and nodes to the blocks might help, but a small search function will likely have the same effect with less pain.

print("Creating loop structures")
# find loops in functions
for i in range(len(graphs)):
    functions[i].graph = copy.deepcopy(graphs[i])
    tempLoops = nx.recursive_simple_cycles(graphs[i])
    listOfLoops = []
    for j in range(len(tempLoops)):
        obLoop = loop()
        obLoop.function = functions[i]
        obLoop.labels = tempLoops[j]
        obLoop.labels = [str(k) for k in obLoop.labels]
        listOfLoops.append(copy.deepcopy(obLoop))
    functions[i].loops = listOfLoops

for fu in functions:
    tempCoLoList = []
    for lo in fu.loops:
        hasBeenAdded = False
        for coLo in tempCoLoList:
            intersec = list(set(coLo.commonLabels).intersection(set(lo.labels)))
            if len(intersec) > 1:
                coLo.commonLabels = intersec
                coLo.loops.append(lo)
                hasBeenAdded = True
        if hasBeenAdded == False:
            newCoLo = collapsedLoop()
            newCoLo.function = fu
            newCoLo.commonLabels = lo.labels
            newCoLo.parallelPaths = []
            newCoLo.loops.append(lo)
            tempCoLoList.append(newCoLo)
    # Do not merge the loops, they have to be sequential because intersec changes. Also, my use of the sets is grounds for court martial.
    for lo in fu.loops:
        for coLo in tempCoLoList:
            if list(set(coLo.commonLabels).intersection(set(lo.labels))) == coLo.commonLabels:
                coLo.parallelPaths.append(list(set(lo.labels)-set(coLo.commonLabels)))
    for coLo in tempCoLoList:
        tempSet = set([])
        for pp in coLo.parallelPaths:
            tempSet = tempSet.union(set(pp))
        coLo.parallelUnion = list(tempSet)
    fu.coLoops = tempCoLoList

if DEBUG == True:
    for fu in functions:
        print(fu.name)
        for lo in fu.loops:
            print(lo)
        for coLo in fu.coLoops:
            print(coLo)
        print("")

# I should remove the loops by this point

print("Collapsing loops sharing common paths")
# This will have to be changed to work with coLoops. The code is getting more spaghettified by the minute.
for fu in functions:
    for coLo in fu.coLoops:
        tempBrList = []
        tempExList = []
        joinLabels = list(set(coLo.commonLabels).union(set(coLo.parallelUnion)))
        for lbl in joinLabels:
            lastInst = getBlockFromLabel(lbl, fu.blocks).instructions[-1]
            splice = lastInst.split(" ");
            # The gotos only have stuff like br label %x, the real branches have more
            if len(splice) > 3:
                newBr = branch()
                newBr.label = lbl
                newBr.splice = splice
                newBr.function = fu
                # Here I need to codify a way of acting with the switches. Also, I'm somehow casting strings to strings just to be on the safe side.
                blo = getBlockFromLabel(lbl, fu.blocks)
                if blo.instructions[-1] == "]":
                    print("SWITCH FOUND in " + fu.name + " at block " + lbl)
                # Sometimes the instruction just above a break is not a comparison. I'm moving upwards until i find one.
                i = -2
                check = getBlockFromLabel(lbl, fu.blocks).instructions[i]
                while(not (check.split(' ')[2] in ["icmp", "fcmp", "phi"])):
                    i -= 1
                    check = getBlockFromLabel(lbl, fu.blocks).instructions[i]
                newBr.condition = check
                if newBr.condition.split(' ')[2] == "icmp" or newBr.condition.split(' ')[2] == "fcmp":
                    newBr.trueLabel = str(newBr.splice[4].translate(None, "%,"))
                    newBr.falseLabel = str(newBr.splice[6].translate(None, "%,"))
                # This is to eliminate once we force a simplifycfg pass
                elif newBr.condition.split(' ')[2] == "phi":
                    print("WARNING! Code with phi nodes is not supported: please run it through opt -simplifycfg. DO NOT trust the results.")

                tempBrList.append(copy.deepcopy(newBr))
                if not (newBr.trueLabel in joinLabels and newBr.falseLabel in joinLabels):
                    tempExList.append(copy.deepcopy(newBr))
        coLo.branches = tempBrList
        coLo.exitBranches = tempExList
        coLo.tempBranches = [eB.label for eB in coLo.exitBranches]
        if DEBUG == True:
            for b in coLo.branches:
                print((b.condition) + " in " + str(lo))
    
    visitedList = []
    for coLo in fu.coLoops:
        # I am only tracing those branches that exit the loop. Otherwise it's just a selection of parallel paths, that's to say an if.
        for bra in coLo.exitBranches:
            # It may also help to use loops, rather than branches. the collapsedloops sharing the branching label, yadda yadda.
            if not bra.label in visitedList:
                visitedList.append(bra.label)
                appearances = 0
                loList = []
                for cL in fu.coLoops:
                    if bra.label in cL.getAllLabels():
                        appearances += 1
                        loList.append(cL)

                # Apparently difference between the exitbranches of the coLoops sharing a branch will give me the right ones to nest. YAY SCIENCE
                if appearances != 1:
                    # If it appears twice, it's the condition of the nested loop.
                    # Here I am tracing branches, and all is well within the world.
                    # Then, either here or later, I will create a forest structure of graphs, all starting from a base_node.
                    # Then I'll trace all the increments recursively
                    minimum = 999999
                    closestLoop = 0
                    for i in range(len(loList)):
                        # One branch can only lead to two different loops. Hopefully.
                        value = list(set(fu.graph.predecessors(bra.label)).intersection(set(loList[i].getAllLabels())))[0]
                        if distToZero(fu, value) < minimum:
                            minimum = distToZero(fu, value)
                            closestLoop = i
                    # Should trigger side effects
                    # Somewhere, I should avoid appending twice a nested loop to its parent (when I trace it in the loop and later. Also, right now I'm tracing twice each branch.
                    
                    tempLoLi = copy.deepcopy(loList[closestLoop].nestedLoops)
                    tempLoLi.append(loList[1-closestLoop])
                    loList[closestLoop].nestedLoops = tempLoLi
                    #(loList[closestLoop].nestedLoops).append(loList[1-closestLoop]) # WHYYYYYYYYYYY

                    # This looks like it's repeated at every iteration. Possibly, better design might help.
                    loList[closestLoop].tempBranches = list(set(loList[closestLoop].tempBranches)-set([bra.label]))
                    loList[1-closestLoop].isNested = True

                    # If criticalBranch were a branch and not an instruction, I could more easily do all my job of checking and nesting, then just trace all the critical branches. Perhaps I should breate and assign one from th label, eh?
                    #print("And tracing " + str(loList[1-closestLoop].commonLabels))
                    #traceCondition(bra, loList[1-closestLoop].commonLabels)
                    '''
                    closeExitBranches = [b.label for b in loList[closestLoop].exitBranches]
                    farExitBranches = [b.label for b in loList[1-closestLoop].exitBranches]
                    outerBranch = list(set(closeExitBranches) - set(farExitBranches))[0]
                    innerBranch = list(set(closeExitBranches).intersection(set(farExitBranches)))[0]
                    print("Outer is " + str(outerBranch) + ", inner is " + str(innerBranch))
                    loList[closestLoop].getExitBranchFromLabel(outerBranch).nestedBranch = loList[closestLoop].getExitBranchFromLabel(innerBranch)

                    traceCondition(bra, coLo)
                    '''

print("Generating tree")
# Looks like it all works, yippe-kee-yay!
output = "\\documentclass[12pt, preview, varwidth=\\maxdimen, border=1cm]{standalone}\n\\usepackage[fleqn]{amsmath}\n\\usepackage{breqn}\n\\setlength{\mathindent}{0pt}\n\\begin{document}\n"
for fu in functions:
    bodylist = [b.label for b in fu.blocks]
    baseNode = treeNode()
    # You know what's fun? The above line does NOT keep default values. I MISS CONSTRUCTORS
    baseNode.object = None
    baseNode.children = []
    for coLo in fu.coLoops:
        # QoL improvement for branches. Pretty sure there is always only one criticalBranch.
        for eBr in coLo.exitBranches:
            if eBr.label == coLo.tempBranches[0]:
                coLo.criticalBranch = copy.deepcopy(eBr)
                coLo.condition = eBr.condition
                traceCondition(eBr, coLo)

        if coLo.isNested == False:
            baseNode.addChild(coLo)
        for lab in coLo.commonLabels + coLo.parallelUnion:
            if lab in bodylist:
                bodylist.remove(lab)
    for chi in baseNode.children:
        addChildren(chi)
    # TEMPORARY HACK
    #print(baseNode)
    #print("")

    inlist = []
    for i in bodylist:
        inlist += getBlockFromLabel(i, fu.blocks).instructions
    bodycalls, bodymemo, bodyarit = traceOpsFromInstr(inlist, fu)

    output += ("\\section{" + fu.name + "}" + "\n")
    output += "\\begin{align*}"
    output += ("\\text{[" + str(bodymemo) + " memory, " + str(bodyarit) + " arithmetic}")
    
    if len(bodycalls) > 0:
        # Maybe count calls?
        output += "\\text{, calls to }"
        for c in bodycalls:
            #print(c)
            output += c.replace("%", "\%").replace("_", "\\_") + "\\text{, }\\end{align*}\\begin{align*}"
        output = output[:-35]
    output += "]\\end{align*}"
    output += (treeTrace(baseNode).replace("%", "\%").replace("_", "\\_") + "\n")
output += "\\end{document}"

print("Output written")

address = "tmpf"
if not os.path.exists(address):
            os.makedirs(address)

out_file = open(address + "/report.tex", "w")
out_file.write(output)
out_file.close()

os.system("pdflatex -output-directory " + address + " " + address + "/report.tex")
shutil.copyfile(address + "/report.pdf", progname + "_report.pdf")
shutil.rmtree(address)

#print(output)
print("Done!")
# TreeTrace is useful, but possibly the best thing would be doing the reverse: count memory and arithmetic instructions in the leaf nodes, then multiply them upwards. This would mean giving the various cLoops a simple string (or better a method) to get to the formula.

# A bonus objective would be to trace the righthand's increase in the function, but then we risk omonymy

#ASSUMPTION: parallel paths have equal probability of being set up.

# For refactoring: check if all those copies are REALLY necessary
